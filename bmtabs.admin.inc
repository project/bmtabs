<?php

/**
 * @file
 * Provides admin functions for bmtabs.
 */

/**
 * Returns FAPI array of bmtabs admin form. Allows users to select which content
 * types should allow which build modes as tabs.
 */
function bmtabs_admin() {

  // Return simple message if no build modes.
  $build_modes = bmtabs_get_build_modes();
  if (!count($build_modes)) {
    return array('bmtabs_no_bm' => array('#value' => t('There are no build modes configured.')));
  }
  
  $form['#submit'][] = 'bmtabs_admin_submit';
  
  // Sort node types alphabetically to make it easy 
  $node_types  = node_get_types();
  uasort($node_types, create_function('$a,$b', 'return strcmp($a->name, $b->name);'));

  $options = array();
  foreach ($build_modes as $mode => $details) {
    $options[$mode] = check_plain($details['title']);
  }

  $form['bmtabs_build_mode_tabs'] = array(
    '#type' => 'checkboxes',
    '#title' => t(' Display tabs for the following build modes', array('!node_type' => $details->name)),
    '#description' => t('These are general settings for all content types.'),
    '#options' => $options,
    '#default_value' => variable_get('bmtabs_build_mode_tabs', array()),
  );
  
  $form['bmtabs_tabs_order']  = _bmtabs_order_tabs_general($build_modes);
  
  $form['bmtabs_tabs_level'] = array(
    '#type' => 'radios',
    '#title' => t('Tab level'),
    '#description' => t("Select which level you'd like the tabs displayed at."),
    '#options' => array(
      BMTABS_PRIMARY => t("Primary level (eg. inline with <em>node/1/edit</em>)"),
      BMTABS_SECONDARY => t("Secondary level (ie. below <em>node/1/view</em>)"),
    ),
    '#default_value' => variable_get('bmtabs_tabs_level', BMTABS_PRIMARY),
  );

  $desc  = 'You can rename the title given to a build mode tab on a specific content type. ';
  $desc .= 'If the content type is excluded, that entry will apply to all content types. ';
  $desc .= 'Machine name should be used for both build mode and node type. ';
  $desc .= 'One entry per line in the following format:<br />';
  $desc .= '[build mode]|[node type - optional]|[tab title]';

  $form['bmtabs_build_mode_rename'] = array(
    '#type' => 'textarea',
    '#title' => t('Rename build mode tab labels'),
    '#description' => t($desc),
    '#default_value' => variable_get('bmtabs_build_mode_rename', null),
  );

  $form['content_types'] = array(
    '#type' => 'fieldset',
    '#title' => t('Build mode tabs per content type'),
  );

  foreach ($node_types as $type => $details) {

    $form['content_types']['bmtabs_build_mode_tabs_' . $type] = array(
      '#type' => 'checkboxes',
      '#title' => t('!node_type: Display tabs for the following build modes', array('!node_type' => $details->name)),
      '#options' => $options,
      '#default_value' => variable_get('bmtabs_build_mode_tabs_' . $type, array()),
    );

  }

  return system_settings_form($form);
}

/**
 * Submit handler for bmtabs_admin: clears the menu router.
 */
function bmtabs_admin_submit($form, &$form_state) {
  module_invoke('menu', 'rebuild');
}

/**
 * Creates the FAPI array for ordering the tabs.
 */
function _bmtabs_order_tabs_general($build_modes) {
  
  $form['#description'] = t('Use the drag-and-drop handles or the values in the weight column to rearrange the order the tabs will be displayed in.');
  $form['#title'] = t('Reorder build mode tabs');

  $order = variable_get('bmtabs_tabs_order', array());
  foreach ($build_modes as $id => $details) {
    $form['names'][$id] = array('#value' => $details['title']);
    
    $form[$id] = array(
      '#type' => 'weight', 
      '#default_value' => isset($order[$id]) ? $order[$id] : BMTABS_DEFAULT_WEIGHT - BMTABS_WEIGHT_OFFSET,
    );
  }
  $form['#tree'] = TRUE;
  $form['#theme'] = 'bmtabs_order_tabs';

  return $form;
}

/**
 * Theme function for the bmtabs order tabs form.
 *
 * @see _bmtabs_order_tabs_general
 * @see drupal_add_tabledrag
 */
function theme_bmtabs_order_tabs($form) {
  $header = array(t('Build mode'), t('Weight'));
  $rows = array();
  foreach(element_children($form['names']) as $id) {
    $form[$id]['#attributes']['class'] = 'bmtabs-order-weight';
    $rows[] = array(
      'data' => array(
        drupal_render($form['names'][$id]),
        drupal_render($form[$id]),
      ),
      'class' => 'draggable',
    );
  }
  
  // In case anything's been added to the form
  $out = drupal_render($form);
  
  $out .= theme('table', $header, $rows, array('id' => 'bmtabs-order'));
  
  // Theme it like a form element with title and description.
  $out = theme('form_element', $form, $out);
  
  drupal_add_tabledrag('bmtabs-order', 'order', 'sibling', 'bmtabs-order-weight');
  
  return $out;
}
